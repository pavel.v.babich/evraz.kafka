<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);

use Bitrix\Main\Localization\Loc;
use Bitrix\Main;

CJSCore::Init();
\Bitrix\Main\UI\Extension::load("ui.vue");
$documentRoot = Main\Application::getDocumentRoot();
$vueTemplates = new Main\IO\Directory($documentRoot . $templateFolder . '/vue-templates');
/** @var Main\IO\File $jsTemplate */
foreach ($vueTemplates->getChildren() as $jsTemplate) {
    include($jsTemplate->getPath());
}
include'icon_templates.php';?>
<?php
//echo '<pre>';
//echo print_r($icons);
//echo '</pre>';?>
<div id="v-app-exhausters">
    <exhausters-list></exhausters-list>
</div>
<script>
    BX.ready(function () {
        var calculator = new BX.Evraz.Exhausters({
            exhausters: <?=CUtil::PhpToJSObject($arResult['MATRIX'], false, true)?>,
            icons: <?=CUtil::PhpToJSObject($icons, false, true)?>,
            lastUpdateTime: "<?=$arResult['DATE_TIME']?>",
            params: <?=CUtil::PhpToJSObject($arParams)?>,
            templateFolder: '<?=CUtil::JSEscape($templateFolder)?>',
            appId: 'v-app-exhausters',
        });
    });
</script>