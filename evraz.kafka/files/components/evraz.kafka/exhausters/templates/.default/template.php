<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);

use Bitrix\Main\Localization\Loc;
use Bitrix\Main;

$documentRoot = Main\Application::getDocumentRoot();
$vueTemplates = new Main\IO\Directory($documentRoot . $templateFolder . '/vue-templates');
?>
<div class="exhausters-block">
<?php
if(!empty($arResult['MATRIX'])){
    foreach($arResult['MATRIX'] as $arExhauster){
        if(!empty($arExhauster['DEVICES']))
        {
        ?>
        <div class="exhauster-item">
            <h3><?php echo $arExhauster['UF_NAME']?> <small><?php echo $arExhauster['UF_DATE_FORMAT']?></small></h3>
            <span class="continuous-work"><?php echo $arExhauster['CONTINUOUS_WORK']?> сут</span><br><br>
            <img src="/upload/exhauster.jpg">
            <?foreach($arExhauster['DEVICES'] as $deviceName => $arParams){?>
                <h4><?php echo $deviceName?></h4>
                <!-- Вывод детальной информации по каждому параметру -->
                <div >
                    <?foreach($arParams as $paramName => $paramVal)
                    {
                        if($paramVal['UF_NAME']){
                            ?><span><?=$paramVal['UF_NAME']?>: <?=$paramVal['VALUE']?></span><br><?php
                        }else{
                            foreach($paramVal as $pCode => $pVal){
                                if(!in_array($pCode, ['alarm_max', 'alarm_min', 'warning_max', 'warning_min'])){
                                    $dopClass = '';
                                    if($paramVal['alarm_max']['VALUE'] && $paramVal['alarm_max']['VALUE'] <= $pVal['VALUE']){
                                        $dopClass = 'alarm';
                                    }
                                    if($paramVal['warning_max']['VALUE'] && $paramVal['warning_max']['VALUE'] <= $pVal['VALUE']){
                                        $dopClass = 'warning';
                                    }
                                    ?><span class="<?=$dopClass?>"><?=$paramName?>: <?=$pVal['VALUE']?><br><?php
                                }
                            }
                        }
                    ?>
                    <?}?>
                </div>
            <?}?>
        </div>
        <?
        }
    }
}
?>
<hr>
<?// ="MATRIX=<pre>".print_r($arResult,1)."</pre>"?>