<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

/** @global CCacheManager $CACHE_MANAGER */

use \Bitrix\Main,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Localization\Loc;

use Evraz\Kafka\Services\Hlblocks;

Loc::loadMessages(__FILE__);

class EvrazKafka extends CBitrixComponent
{
    /**
     * @var Hlblocks
     */
    private $HlBlockHandler;

    public function __construct($component = null)
    {
        global $APPLICATION;
        $APPLICATION->SetTitle("Прогнозная аналитика эксгаустеров");

        try {
            $this->checkModules();
        } catch (\Exception $e) {

        }

        CJSCore::Init(array('fx', 'popup', 'window', 'ajax', 'currency', 'ui.vue'));
        \Bitrix\Main\UI\Extension::load("ui.vue");
        parent::__construct($component);

    }

    /**
     * @throws Main\LoaderException
     */
    protected function checkModules()
    {
        if (!Loader::includeModule('iblock')) {
            throw new Main\LoaderException('not install module iblock');
        };

        if (!Loader::includeModule('evraz.kafka')) {
            throw new Main\LoaderException('not install module evraz.kafka');
        };

        if (!Loader::includeModule('highloadblock')) {
            throw new Main\LoaderException('not install module highloadblock');
        };

    }

    protected function getData()
    {
        $this->HlBlockHandler = new Hlblocks();
        $this->arResult['EXHAUSTER_LIST'] = $this->HlBlockHandler->arExhausters;

        $arRawData = $this->HlBlockHandler->getLastData();

        $this->arResult['DATE_TIME'] = $arRawData['DATETIME'];

        $arMatrix = $this->NormalizeExhaustersDataForComponent($arRawData['DATA']);


        $this->arResult['MATRIX'] = $arMatrix;
    }

    public function executeComponent()
    {

        $this->getData();

        $this->includeComponentTemplate();

    }

    public function NormalizeExhaustersDataForComponent(array $arData)
    {

        $arResult = $this->HlBlockHandler->arExhausters;
        $arExhaustersMapping = $this->HlBlockHandler->arExhaustersMapping ;
        foreach($arResult as &$arExhauster){
            foreach($arData as $arItem){
                $exhausterId = $arExhaustersMapping[$arItem['UF_KAFKA_CODE']]['UF_EXHAUSTER'];
                if($arExhauster['ID'] == $exhausterId){
                    $deviceName = $arExhaustersMapping[$arItem['UF_KAFKA_CODE']]['UF_DEVICE_NAME'];
                    $ParamName = $arExhaustersMapping[$arItem['UF_KAFKA_CODE']]['UF_PARAM_NAME'];
                    $arChar = [
                        'UF_PARAM_CHAR' => $arExhaustersMapping[$arItem['UF_KAFKA_CODE']]['UF_PARAM_CHAR'],
                        'UF_CODE' => $arExhaustersMapping[$arItem['UF_KAFKA_CODE']]['UF_CODE'],
                        'UF_KAFKA_CODE' => $arItem['UF_KAFKA_CODE'],
                        'UF_NAME' => $arExhaustersMapping[$arItem['UF_KAFKA_CODE']]['UF_NAME'],
                        'UF_TYPE' => $arExhaustersMapping[$arItem['UF_KAFKA_CODE']]['UF_TYPE'],
                        'UF_ACTIVE' => $arExhaustersMapping[$arItem['UF_KAFKA_CODE']]['UF_ACTIVE'],
                        'VALUE' => $arItem['UF_VALUE'],
                    ];
                    $arExhauster['DEVICES'][$deviceName]['name'] = $deviceName;

                     //для подшипников добавляется параметр для подсвечивания на схеме
                    if(mb_stripos($deviceName,'Подшипник') !== false){
                        $arExhauster['DEVICES'][$deviceName]['scheme_part'] = 'part-' . str_replace('Подшипник ', '', $deviceName);
                    }
                    if(mb_stripos($arChar['UF_NAME'],'температура') !== false){
                        $arChar['ICON_TYPE'] = 'temperature';
                    }
                    if(mb_stripos($arChar['UF_NAME'],'вибрация') !== false){
                        $arChar['ICON_TYPE'] = 'vibration';
                    }
                    if(mb_stripos($arChar['UF_NAME'],'масло') !== false){
                        $arChar['ICON_TYPE'] = 'щшд';
                    }

                    if(mb_strtolower($ParamName) === 'вода' ||
                        mb_strtolower($ParamName) === 'масло' ||
                        !$ParamName
                    ){
                        $arExhauster['DEVICES'][$deviceName]['measures'][$arChar['UF_CODE']] = $arChar;
                    } elseif(mb_strtolower($ParamName) === 'вибрация') {
                        $ParamName = $ParamName . " " . mb_strtolower($arChar['UF_PARAM_CHAR']);
                        $arExhauster['DEVICES'][$deviceName]['measures'][$ParamName][$arChar['UF_CODE']] = $arChar;
                    } else {
                        $arExhauster['DEVICES'][$deviceName]['measures'][$ParamName][$arChar['UF_CODE']] = $arChar;
                        if(!in_array($arChar['UF_CODE'], ['alarm_max','alarm_min','warning_max','warning_min'])){
                            $arExhauster['DEVICES'][$deviceName]['measures'][$ParamName]['value'] = $arItem['UF_VALUE'];
                        }
                    }

                    //Статус работы
                    if($arChar['UF_CODE'] == 'work'){
                        $arExhauster['IS_WORK'] = $arChar['VALUE'];
                    }
                }
            }
        }
        unset($arExhauster);
        return $arResult;
    }
}
