<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;
Loader::includeModule("evraz.kafka");

use Evraz\Kafka\Services\Graphs;
use Evraz\Kafka\Services\Hlblocks;
use Bitrix\Main\Application; 

$request = Application::getInstance()->getContext()->getRequest();
$arVals = $request->getValues();
if($arVals['FILTER']){
    $HlBlockHandler = new Hlblocks();
    $arMapping = $HlBlockHandler->arExhaustersMapping;
    $zoom = $arVals['zoom'];
    $arSetParams = array_keys($arVals['FILTER']);
    foreach($arSetParams as &$param){
        $param = urldecode($param);
    }
    unset($param);

    $Graphs = new Graphs();
    $arResult = $Graphs->getGraphJson($arSetParams, $zoom);

    $k = 0;
    foreach($arResult["DATA"] as $kafkaCode => $arParams){
        $arResult["trade"][$k] = ["label" => $arMapping[$kafkaCode]["UF_NAME"]];
        foreach($arParams as $arParam){
            $arResult['trade'][$k]['data'][] = [$arParam['TIMESTAMP'] * 1000, $arParam['UF_VALUE']];
        }
        $k++;
    }

    $arResult['markings'] = [
          [
            "color" => "#f00",
            "lineWidth" => 2,
            "yaxis" => ["from" => 0.4, "to" => 0.4 ]
          ],
      ];

    unset($arResult["DATA"]);

    echo json_encode($arResult, JSON_UNESCAPED_UNICODE);

}else{
    echo "Выберите параметры для контроля";
}



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");