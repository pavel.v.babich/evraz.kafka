<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);

use Bitrix\Main\Localization\Loc;
use Bitrix\Main;

$documentRoot = Main\Application::getDocumentRoot();
$templatesPath = new Main\IO\Directory($documentRoot . $templateFolder . '/vue-templates');
?>
<script language="javascript" type="text/javascript" src="<?=$templateFolder?>/flot-master/lib/globalize.js"></script>
<script language="javascript" type="text/javascript" src="<?=$templateFolder?>/flot-master/source/jquery.js"></script>
<script language="javascript" type="text/javascript" src="<?=$templateFolder?>/flot-master/source/jquery.canvaswrapper.js"></script>
<script language="javascript" type="text/javascript" src="<?=$templateFolder?>/flot-master/source/jquery.colorhelpers.js"></script>
<script language="javascript" type="text/javascript" src="<?=$templateFolder?>/flot-master/source/jquery.flot.js"></script>
<script language="javascript" type="text/javascript" src="<?=$templateFolder?>/flot-master/source/jquery.flot.saturated.js"></script>
<script language="javascript" type="text/javascript" src="<?=$templateFolder?>/flot-master/source/jquery.flot.browser.js"></script>
<script language="javascript" type="text/javascript" src="<?=$templateFolder?>/flot-master/source/jquery.flot.drawSeries.js"></script>
<script language="javascript" type="text/javascript" src="<?=$templateFolder?>/flot-master/source/jquery.flot.uiConstants.js"></script>
<script language="javascript" type="text/javascript" src="<?=$templateFolder?>/flot-master/source/jquery.flot.time.js"></script>
<script language="javascript" type="text/javascript" src="<?=$templateFolder?>/flot-master/source/jquery.flot.symbol.js"></script>
<script language="javascript" type="text/javascript" src="<?=$templateFolder?>/flot-master/source/jquery.flot.legend.js"></script>

<div class="exhausters-trends-page">
<div class="exhausters-trends">
<form id="exhauster-params-filter">
<?php
if(!empty($arResult['MATRIX'])){
    foreach($arResult['MATRIX'] as $arExhauster){
        if(!empty($arExhauster['DEVICES']))
        {
        ?>
            <input
                id="exhauster-switcher-<?=$arExhauster['UF_CODE']?>"
                class="exhauster-item-switcher"
                type="checkbox" name="<?=$arExhauster['UF_CODE']?>"
            >
            <div class="exhauster-item">
                <div class="exhauster-item-open-block">
                    <label
                        class="exhauster-switcher-tab"
                        for="exhauster-switcher-<?=$arExhauster['UF_CODE']?>"
                    ></label>
                    <label
                        class="exhauster-switcher-label"
                        for="exhauster-switcher-<?=$arExhauster['UF_CODE']?>"
                    ><?php echo $arExhauster['UF_NAME']?></label>
                </div>
                <div class="exhauster-item-devices">
                    <?
                    $deviceKey = 0;
                    foreach($arExhauster['DEVICES'] as $deviceName => $arParams){
                        $deviceKey++;
                        ?>
                        <input
                            id="exhauster-<?=$arExhauster['UF_CODE']?>-device-<?=$deviceKey?>"
                            class="exhauster-item-device-switcher"
                            type="checkbox" name="<?=$arExhauster['UF_CODE']?>"
                        >
                        <div class="exhauster-item-device-block">
                            <div class="exhauster-item-open-block">
                                <label
                                    class="exhauster-item-device-switcher-tab"
                                    for="exhauster-<?=$arExhauster['UF_CODE']?>-device-<?=$deviceKey?>"
                                ></label>
                                <label
                                    class="exhauster-item-device-switcher-label"
                                    for="exhauster-<?=$arExhauster['UF_CODE']?>-device-<?=$deviceKey?>"
                                ><?php echo $deviceName?></label>
                            </div>
                            <div class="exhauster-item-device-parametrs">
                                <?foreach($arParams as $paramName => $paramVal)
                                {
                                    if($paramVal['UF_NAME']){
                                        ?>
                                            <div class="exhauster-item-device-parametr-block">
                                                <input
                                                    class="js-trend-parametr"
                                                    id="<?=$paramVal['UF_KAFKA_CODE']?>"
                                                    type="checkbox"
                                                    name="FILTER[<?=urlencode($paramVal['UF_KAFKA_CODE'])?>]"
                                                    value="Y"
                                                >
                                                <label
                                                    for="<?=$paramVal['UF_KAFKA_CODE']?>"
                                                    class="<?=$dopClass?>"
                                                ><?=$paramVal['UF_NAME']?></label>
                                                <span class="exhauster-item-device-parametr-value"><?=round($paramVal['VALUE'], 3)?></span>
                                            </div>
                                        <?php
                                    }else{
                                        foreach($paramVal as $pCode => $pVal){
                                            if(!in_array($pCode, ['alarm_max', 'alarm_min', 'warning_max', 'warning_min'])){
                                                $dopClass = '';
                                                if($paramVal['alarm_max']['VALUE'] && $paramVal['alarm_max']['VALUE'] <= $pVal['VALUE']){
                                                    $dopClass = 'alarm';
                                                }
                                                if($paramVal['warning_max']['VALUE'] && $paramVal['warning_max']['VALUE'] <= $pVal['VALUE']){
                                                    $dopClass = 'warning';
                                                }
                                                ?>
                                                    <div class="exhauster-item-device-parametr-block">
                                                        <input
                                                            class="js-trend-parametr"
                                                            id="<?=$pVal['UF_KAFKA_CODE']?>"
                                                            type="checkbox" name="FILTER[<?=urlencode($pVal['UF_KAFKA_CODE'])?>]"
                                                            value="Y"
                                                        >
                                                        <label
                                                            for="<?=$pVal['UF_KAFKA_CODE']?>"
                                                            class="<?=$dopClass?>"
                                                        ><?=$paramName?></label>
                                                        <span class="exhauster-item-device-parametr-value"><?=round($pVal['VALUE'], 3)?></span>
                                                    </div>
                                                <?php
                                            }
                                        }
                                    }
                                ?>
                                <?}?>
                            </div>
                        </div>
                    <?}?>
                </div>
            </div>
        <?
        }
    }
}
?>
</form>
</div>
<div id="content">
    <div class="exhausters-trends-content-header">
        <div id="period-block"></div>
        <div class="zoom-block">
            <select id="select-zoom" name="zoom">
                <option value="1">10 мин</option>
                <option value="2" selected="selected">60 мин</option>
                <option value="3">4 часа</option>
                <option value="4">16 часов</option>
                <option value="5">1 день</option>
            </select>
        </div>
    </div>
    <div class="demo-container">
        <div id="placeholder" class="demo-placeholder"></div>
        <div id="legendContainer" class="legend" style="width:9em;height:8em;border-style:solid;border-color:threedface"></div>
    </div>
    <div id="trends-result"></div>
</div>
</div>