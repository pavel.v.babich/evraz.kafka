<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

/** @global CCacheManager $CACHE_MANAGER */

use \Bitrix\Main,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Localization\Loc;

use Evraz\Kafka\Services\Hlblocks;

Loc::loadMessages(__FILE__);

class EvrazKafka extends CBitrixComponent
{
    public function __construct($component = null)
    {
        global $APPLICATION;
        $APPLICATION->SetTitle("График состояния эксгаустеров");

        try {
            $this->checkModules();
        } catch (\Exception $e) {

        }

        CJSCore::Init(array('fx', 'popup', 'window', 'ajax', 'currency', 'ui.vue'));
        \Bitrix\Main\UI\Extension::load("ui.vue");
        parent::__construct($component);

    }

    /**
     * @throws Main\LoaderException
     */
    protected function checkModules()
    {
        if (!Loader::includeModule('iblock')) {
            throw new Main\LoaderException('not install module iblock');
        };

        if (!Loader::includeModule('evraz.kafka')) {
            throw new Main\LoaderException('not install module evraz.kafka');
        };

        if (!Loader::includeModule('highloadblock')) {
            throw new Main\LoaderException('not install module highloadblock');
        };

    }

    protected function getData()
    {
        $HlBlockHandler = new Hlblocks();
        $this->arResult['EXHAUSTER_LIST'] = $HlBlockHandler->arExhausters;

        $arRawData = $HlBlockHandler->getLastData();

        $this->arResult['DATE_TIME'] = $arRawData['DATETIME'];

        $arMatrix = $HlBlockHandler->NormalizeExhaustersData($arRawData['DATA']);

        $this->arResult['MATRIX'] = $arMatrix;
    }

    public function executeComponent()
    {

        $this->getData();

        $this->includeComponentTemplate();

    }
}
