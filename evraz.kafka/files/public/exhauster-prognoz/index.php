<?php
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');

/** @var \CMain $APPLICATION */

use Bitrix\Main\Loader;
use Evraz\Kafka\Services\Hlblocks;

Loader::includeModule("evraz.kafka");

global $APPLICATION;
$APPLICATION->SetTitle("Прогноз замены роторов эксгаустеров");

$HlBlockHandler = new Hlblocks();

$arRawData = $HlBlockHandler->getLastData();
$arMatrix = $HlBlockHandler->NormalizeExhaustersData($arRawData['DATA']);

$arMain = [];
$arAllControlCodes = [];
foreach($arMatrix as $eksgausterCode => $arEksgauster){
    $maxDays = 99;
    echo "<h3 style='margin: 0;'>Начинаем анализ: ".$arEksgauster['UF_NAME']."</h3><br>";
    echo "С момента установки ротора: " . $arEksgauster['CONTINUOUS_WORK']."дней/дня<br>";
    $fromDateStamp = $arEksgauster['UF_DATE']->getTimestamp() + 3600 * 24;
    $fromDate = date('d.m.Y H:i:s', $fromDateStamp);
    $DateID = $HlBlockHandler->getDayDateID($fromDate);
    $arEksgausterControlParams = [];
    foreach($arEksgauster['DEVICES'] as $deviceName => $arParams){
        foreach($arParams as $paramName => $arChars){
            if($arChars["UF_KAFKA_CODE"])
                continue;

            $arControl = [];
            foreach($arChars as $charCode => $arItem){
                if(in_array($charCode, ['alarm_max', 'alarm_min', 'warning_max', 'warning_min'])){
                    $arControl[$charCode] = $arItem['VALUE'];
                }else{
                    $arControl["KAFKA_CODE"] = $arItem['UF_KAFKA_CODE'];
                    $arControl["CURRENT_VAL"] = $arItem['VALUE'];
                    $arControl["NAME"] = $arItem['UF_NAME'];
                }
            }
            if($arControl['alarm_max'] &&
                $arControl["KAFKA_CODE"]
            ){
                $arEksgausterControlParams[] = $arControl;
                $arAllControlCodes[$arControl["KAFKA_CODE"]] = $eksgausterCode;
                if(mb_strtolower(mb_substr($arControl['NAME'], 0, 33)) == 'горизонтал вибрация подшипника №7' ||
                    mb_strtolower(mb_substr($arControl['NAME'], 0, 33)) == 'горизонтал вибрация подшипника №8' ||
                    mb_strtolower(mb_substr($arControl['NAME'], 0, 31)) == 'вертикал вибрация подшипника №7' ||
                    mb_strtolower(mb_substr($arControl['NAME'], 0, 31)) == 'вертикал вибрация подшипника №8'
                )
                {
                    echo "<b>Анализируем динамику устройства: ".$arControl['NAME'].";</b><br>";
                    $arSignal = $HlBlockHandler->getSignal($DateID, $arControl['KAFKA_CODE']);

                    if($arControl['CURRENT_VAL'] > $arSignal['UF_VALUE']){
                        echo "Среднее значение параметра увеличилось на " . ($arControl['CURRENT_VAL'] - $arSignal['UF_VALUE'])."<br>";
                        $speed = ($arControl['CURRENT_VAL'] - $arSignal['UF_VALUE']) / $arEksgauster['CONTINUOUS_WORK'];
                        $prognoz = round( ($arControl['warning_max'] - $arControl['CURRENT_VAL']) / $speed);
                        if($maxDays > $prognoz){
                            $maxDays = $prognoz;
                        }

                        echo "Средняя скорость:" . $speed . ' / в день<br>';
                        echo "Уровня предупреждения(warning_max):" . $arControl['warning_max'] . "<br>";
                        echo "Параметр достигнет через:". $prognoz . "дней/дня" . "<br>";
                    } else {
                        echo "Параметр существенно не изменился";
                    }
                }
            }
        }
    }
    $arMain[$eksgausterCode] = $arEksgausterControlParams;
    echo "<b style='color:red;'>Прогноз по &laquo;".$arEksgauster['UF_NAME']."&raquo : ".$maxDays."</b><br>";
    if($maxDays == 99){
        echo "<b style='color:red;'>(по умолчанию)</b><br>";
    }

    $HlBlockHandler->UpdateEksgausterPrognoz($arEksgauster['ID'], $maxDays);

    echo "<hr><hr>";
}

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
