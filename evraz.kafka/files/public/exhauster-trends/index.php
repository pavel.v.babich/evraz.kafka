<?php

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');

/** @var \CMain $APPLICATION */

$APPLICATION->includeComponent(
  'evraz.kafka:exhaustersgraphs',
  'trends'
);

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
