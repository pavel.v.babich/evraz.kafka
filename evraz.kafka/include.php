<?php

use Bitrix\Main\ArgumentNullException;
use Bitrix\Main\ArgumentOutOfRangeException;
use Bitrix\Main\Config\Option;

/**
 * @class EvrazKafkaSettings
 * содержит вспомогательные методы для работы с кафкой.
 *
 * @see get - возвращает параметр
 * @see set - устанавливает параметр
 * @see getModule - возвращает название модуля
 *
 */
class EvrazKafkaSettings
{

    /**
     * @var array
     */
    protected static $params = [];
    /**
     * @var string
     */
    protected static $module = 'evraz.kafka';
    /**
     * @var string
     */
    public static $settings_name = 'EVRAZ_KAFKA_SETTING';

    public static $paramsNames = [
        'KAFKA_URL_BROKER',
        'KAFKA_MECHANISM',
        'KAFKA_PROTOCOL',
        'KAFKA_CERT_LOCATION',
        'CONSUMER_USER_NAME',
        'CONSUMER_PASSWORD',
        'LOG_ACTIVE',
        'KAFKA_TOPIC_PRICE_LIST',
        'KAFKA_GROUP_ID_PRICE_LIST',
    ];

    /**
     * Возвращает параметр
     * @param $key
     * @param mixed $def
     * @return mixed|string
     * @throws ArgumentNullException
     * @throws ArgumentOutOfRangeException
     */
    public static function get($key, $def = '')
    {
        $opt = Option::get(self::getModule(), $key, $def);
        return $opt;
    }

    /**
     * Устанавливает параметр
     * @param $key
     * @param $value
     * @throws ArgumentOutOfRangeException
     */
    public static function set($key, $value): void
    {
        Option::set(self::getModule(), $key, $value);
    }

    /**
     * Возвращает название модуля
     * @return string
     */
    public static function getModule(): string
    {
        return self::$module;
    }

    /**
     * Возвращает параметр или значение по умолчанию без генерации исключений.
     * @param $key
     * @param $def
     * @return mixed|string
     */
    public static function getParamOrDefault($key, $def)
    {
        try {
            $opt = self::get($key, $def);
        } catch (Exception $e) {
            $opt = $def;
        }

        return $opt;
    }

    /**
     * Метод save сохраняет данные
     */
    public static function save(): void
    {
        $arVals = $_POST[self::$settings_name];
        if($arVals && !isset($arVals['LOG_ACTIVE'])){
            $arVals['LOG_ACTIVE'] = '';
        }
        foreach (self::$paramsNames as $param) {
            if (isset($arVals[$param])) {
                try {
                    self::set($param, $arVals[$param]);
                } catch (ArgumentOutOfRangeException $e) {
                }
            }
        }
    }
}