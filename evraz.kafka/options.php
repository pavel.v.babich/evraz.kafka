<?php
/*Bitrix\Main\Loader::registerAutoloadClasses(
    "evraz.kafka",
    array(
        "Evraz\\Kafka\\Services\\Auth" => "lib/services/auth.php",
    )
);*/

define("ADMIN_MODULE_NAME", "evraz.kafka");

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main\Config\Option,
    Bitrix\Main\Loader;

Loader::includeModule(ADMIN_MODULE_NAME);
IncludeModuleLangFile(__FILE__);
IncludeModuleLangFile(__FILE__, "ru");
$APPLICATION->SetTitle(GetMessage("PS_MAIN_MODULE_PAGE_SETTINGS"));
if (!$USER->IsAdmin()) $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

if ($REQUEST_METHOD == "POST" && $_REQUEST["save"] && check_bitrix_sessid()) {
    EvrazKafkaSettings::save();
}

require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/prolog_admin_after.php");
if ($USER->IsAdmin()) {
    ?>
    <style>
        a#bx-evraz-indicator {
            background: #888;
            display: inline-block !important;
            width: 50px;
            height: 15px;
            position: relative;
            border-radius: 8px;
            margin: 12px 3px;
            top: 10px;
        }

        a#bx-evraz-indicator.evraz-on {
            background: #070;
        }

        span#bx-evraz-toggle-icon {
            width: 24px;
            height: 24px;
            display: block;
            position: absolute;
            top: -4px !important;
            background: scroll transparent url(/bitrix/js/main/core/images/panel/top-panel-sprite-2.png) no-repeat -284px -1721px !important;
            left: -2px;
        }

        .evraz-on span#bx-evraz-toggle-icon {
            left: 35px !important;
        }

        .evraz-hint {
            cursor: pointer;
            position: relative;
            border: 1px solid #777;
            background: #bbb;
            padding: 0px 2px;
            color: #fff;
            font-weight: 600;
            border-radius: 20px;
            font-size: 8px;
            top: -6px;
            left: 3px;
        }

        .eset-hint {
            cursor: pointer;
            position: relative;
            border: 1px solid #777;
            background: #bbb;
            padding: 0px 2px;
            color: #fff;
            font-weight: 600;
            border-radius: 20px;
            font-size: 8px;
            top: -6px;
            left: 3px;
        }
    </style>
    <form method="POST" Action="<?= $APPLICATION->GetCurPage() ?>?mid=<?= ADMIN_MODULE_NAME ?>"
          ENCTYPE="multipart/form-data" name="settings_form">
        <?= bitrix_sessid_post() ?>
        <table>
            <!-- Данные для авторизации в сервисе Aplaut -->
            <tr>
                <td colspan="2"><h2><?= GetMessage("PS_MODULE_KAFKA_TITLE") ?></h2></td>
            </tr>
            <tr>
                <td width="40%">
                    <?= GetMessage("PS_MODULE_KAFKA_URL_BROKER") ?>:<br>
                </td>
                <td width="60%">
                    <input
                        style="width: 100%"
                        type="text"
                        maxlength="100"
                        name="EVRAZ_KAFKA_SETTING[KAFKA_URL_BROKER]"
                        value="<?= EvrazKafkaSettings::get("KAFKA_URL_BROKER") ?>">
                </td>
            </tr>
            <tr>
                <td width="40%">
                    <?= GetMessage("PS_MODULE_KAFKA_MECHANISM") ?>:<br>
                </td>
                <td width="60%">
                    <input
                        style="width: 100%"
                        type="text"
                        maxlength="100"
                        name="EVRAZ_KAFKA_SETTING[KAFKA_MECHANISM]"
                        value="<?= EvrazKafkaSettings::get("KAFKA_MECHANISM") ?>">
                </td>
            </tr>
            <tr>
                <td width="40%">
                    <?= GetMessage("PS_MODULE_KAFKA_PROTOCOL") ?>:<br>
                </td>
                <td width="60%">
                    <input
                        style="width: 100%"
                        type="text"
                        maxlength="100"
                        name="EVRAZ_KAFKA_SETTING[KAFKA_PROTOCOL]"
                        value="<?= EvrazKafkaSettings::get("KAFKA_PROTOCOL") ?>">
                </td>
            </tr>
            <tr>
                <td width="40%">
                    <?= GetMessage("PS_MODULE_KAFKA_CERT_LOCATION") ?>:<br>
                </td>
                <td width="60%">
                    <input
                        style="width: 100%"
                        type="text"
                        maxlength="100"
                        name="EVRAZ_KAFKA_SETTING[KAFKA_CERT_LOCATION]"
                        value="<?= EvrazKafkaSettings::get("KAFKA_CERT_LOCATION") ?>">
                </td>
            </tr>
            <tr>
                <td width="40%">
                    <?= GetMessage("PS_MODULE_CONSUMER_USER_NAME") ?>:<br>
                </td>
                <td width="60%">
                    <input
                        style="width: 100%"
                        type="text"
                        maxlength="100"
                        name="EVRAZ_KAFKA_SETTING[CONSUMER_USER_NAME]"
                        value="<?= EvrazKafkaSettings::get("CONSUMER_USER_NAME") ?>">
                </td>
            </tr>
            <tr>
                <td width="40%">
                    <?= GetMessage("PS_MODULE_CONSUMER_PASSWORD") ?>:<br>
                </td>
                <td width="60%">
                    <input
                            style="width: 100%"
                            type="text"
                            maxlength="100"
                            name="EVRAZ_KAFKA_SETTING[CONSUMER_PASSWORD]"
                            value="<?= EvrazKafkaSettings::get("CONSUMER_PASSWORD") ?>">
                </td>
            </tr>
            <tr>
                <td width="40%">
                    <?= GetMessage("PS_MODULE_KAFKA_GROUP_ID_PRICE_LIST") ?>:<br>
                </td>
                <td width="60%">
                    <input
                            style="width: 100%"
                            type="text"
                            maxlength="100"
                            name="EVRAZ_KAFKA_SETTING[KAFKA_GROUP_ID_PRICE_LIST]"
                            value="<?= EvrazKafkaSettings::get("KAFKA_GROUP_ID_PRICE_LIST") ?>">
                </td>
            </tr>
            <tr>
                <td width="40%">
                    <?= GetMessage("PS_MODULE_KAFKA_TOPIC_PRICE_LIST") ?>:<br>
                </td>
                <td width="60%">
                    <input
                            style="width: 100%"
                            type="text"
                            maxlength="100"
                            name="EVRAZ_KAFKA_SETTING[KAFKA_TOPIC_PRICE_LIST]"
                            value="<?= EvrazKafkaSettings::get("KAFKA_TOPIC_PRICE_LIST") ?>">
                </td>
            </tr>
            <tr>
                <td width="40%">
                    <?= GetMessage("PS_MODULE_CONSUMER_LOG_ACTIVE") ?>:<br>
                </td>
                <td width="60%">
                    <?$LOG_ACTIVE = EvrazKafkaSettings::get("LOG_ACTIVE");?>
                    <input
                            style="width: 20px;height: 20px;"
                            type="checkbox"
                            <?if($LOG_ACTIVE === "Y"){?>checked="checked"<?}?>
                            name="EVRAZ_KAFKA_SETTING[LOG_ACTIVE]"
                            value="Y">
                </td>
            </tr>
            <tr>
                <td width="40%">
                    <a href="/bitrix/admin/fileman_admin.php?PAGEN_1=1&SIZEN_1=20&lang=ru&site=s1&path=%2Fupload%2Fkafka_log&show_perms_for=0"
                       target="_blank"
                    ><?= GetMessage("PS_MODULE_CONSUMER_LOG_FILE") ?></a>
                </td>
                <td width="60%">
                </td>
            </tr>
            <tr>
                <td width="40%" colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td width="40%">
                    <button type="submit" name="save" value="Y">Сохранить</button>
                </td>
                <td width="60%"></td>
            </tr>
        </table>
    </form>
    <?
}

if ($ex = $APPLICATION->GetException()) {
    $message = new CAdminMessage(GetMessage("rub_save_error"), $ex);
    echo $message->Show();
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>