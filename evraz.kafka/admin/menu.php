<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();
$aMenu = array(
    array(
        'parent_menu' => 'global_menu_settings',
        "icon" => "menu_icon",
        "page_icon" => "default_page_icon",
        'sort' => 900,
        'text' => "EVRAZ Kafka",
        'title' => "EVRAZ Kafka",
        'url' => '/bitrix/admin/evraz_kafka_settings.php'
    )
);
return $aMenu;