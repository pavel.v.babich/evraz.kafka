<?php
namespace Evraz\Kafka\Services;

use Bitrix\Main\Loader;
use Evraz\Kafka\Services\Hlblocks;

/**
 * @class Excel
 *
 * @package Evraz\Kafka\Services
 */
class Excel
{
    /**
    * Путь к классу PHPExcel
    */
    private $ExcelModulePath = '/bitrix/modules/nkhost.phpexcel/lib/PHPExcel/Classes_overload0';

    /**
    * Путь к файлу Маппинг сигналов.xlsx
    */
    private $mappingFilePath = '/local/modules/evraz.kafka/files/mapping.xlsx';

    public function __construct($ExcelModulePath = false)
    {
        global $PHPEXCELPATH;
        $PHPEXCELPATH = $_SERVER['DOCUMENT_ROOT'] . $this->ExcelModulePath;
    }

    /**
    * Загрузка файла Маппинг сигналов
    */
    public function loadMappingSignals($filePath = false)
    {
        Loader::includeModule("evraz.kafka");
        global $PHPEXCELPATH;

        if(!$filePath){
            $filePath = $this->mappingFilePath;
        }

        // Ваш код далее
        require_once ($PHPEXCELPATH . '/PHPExcel/IOFactory.php');
        $xls = \PHPExcel_IOFactory::load($_SERVER['DOCUMENT_ROOT'].$filePath);

        $HlBlockHandler = new Hlblocks();

        $arSheetNames = $xls->getSheetNames();
        $HlBlockHandler->LoadHlExhauster($arSheetNames);

        foreach($arSheetNames as $SheetIndex => $ExhausterName){
            $ExhausterCode = \Cutil::translit($ExhausterName, "ru");
            if($HlBlockHandler->arExhausters[$ExhausterCode]){
                $RawExhausterData = [];
                $ExhausterID = $HlBlockHandler->arExhausters[$ExhausterCode]['ID'];

                // Устанавливаем индекс активного листа
                $xls->setActiveSheetIndex($SheetIndex);
                // Получаем активный лист
                $sheet = $xls->getActiveSheet();

                for ($i = 1; $i <= $sheet->getHighestRow(); $i++) {
                    $nColumn = \PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());
                    for ($j = 0; $j < $nColumn; $j++) {
                        $RawExhausterData[$i][$j] = $sheet->getCellByColumnAndRow($j, $i)->getValue();
                    }
                }

                $arExhausterMapping = $this->NormalizeExhausterData($ExhausterID, $RawExhausterData);
                $HlBlockHandler->LoadExhausterMapping($arExhausterMapping);
            }
        }
    }

    public function NormalizeExhausterData(int $ExhausterID, array $arExhausterData) : array
    {
        $arData = [];
        if($ExhausterID > 0){
            foreach($arExhausterData as $rId => $rawRowData){
                if($rId < 2)
                    continue;
                if(trim($rawRowData[0])){
                    $deviceName = $rawRowData[0];
                    $paramName = $paramChar = '';
                }
                if(trim($rawRowData[1])){
                    $paramName = $rawRowData[1];
                }
                if(trim($rawRowData[2]) &&
                    mb_strtolower(trim($rawRowData[2])) !== "уставки"
                )
                {
                    $paramChar = $rawRowData[2];
                }
                if(trim($rawRowData[3])){
                    $paramCharCode = $rawRowData[3];
                }
                if(trim($rawRowData[4])){
                    $KafkaCode = $rawRowData[4];
                }
                if(trim($rawRowData[5])){
                    $paramCharName = $rawRowData[5];
                }
                if(trim($rawRowData[6])){
                    $paramCharType = $rawRowData[6];
                }
                if(trim($rawRowData[7])){
                    $paramCharActive = $rawRowData[7];
                }
                if($deviceName)
                {
                    $arData[] = [
                        "UF_EXHAUSTER" => $ExhausterID,
                        "UF_DEVICE_NAME" => $deviceName,
                        "UF_PARAM_NAME" => $paramName,
                        "UF_PARAM_CHAR" => $paramChar,
                        "UF_CODE" => $paramCharCode,
                        "UF_KAFKA_CODE" => $KafkaCode,
                        "UF_NAME" => $paramCharName,
                        "UF_TYPE" => $paramCharType,
                        "UF_ACTIVE" => $paramCharActive
                    ];
                }
            }
        }
        return $arData;
    }
}