<?php

namespace Evraz\Kafka\Services\Contracts;

/**
 * Тело продюсера
 */
interface ProducerInterface
{

    /**
     * @param array $arMessage
     * @param string $sKey
     * @return void
     */
    public function send(array $arMessage, string $sKey) : void;

}