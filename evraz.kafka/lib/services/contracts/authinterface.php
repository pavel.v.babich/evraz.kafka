<?php

namespace Evraz\Kafka\Services\Contracts;

/**
 * Тело авторизации
 */
interface AuthInterface
{

    /**
     * @param string $sType
     * @return mixed
     */
    public function setType(string $sType);

    /**
     * @return mixed
     */
    public function getType();

    /**
     * @return mixed
     */
    public function register();

}