<?php

namespace Evraz\Kafka\Services\Contracts;

use RdKafka\KafkaConsumer;

/**
 * Тело консьюмера
 */
interface ConsumerInterface
{

    public function read(KafkaConsumer $oConsumer) : ? array;

}