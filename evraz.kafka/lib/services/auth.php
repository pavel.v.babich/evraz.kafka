<?php

namespace Evraz\Kafka\Services;

use RdKafka\Conf;
use Evraz\Kafka\Services\Contracts\AuthInterface;
use EvrazKafkaSettings;

/**
 * @class Auth
 *
 * @package Evraz\Kafka\Services
 */
class Auth implements AuthInterface
{

    /**
     * @var string
     */
    private $sTypeConnect;

    public function __construct(string $sType)
    {
        $this->setType($sType);
    }

    /**
     * Авторизация кафки
     * @return Conf
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    public function register() : Conf
    {
        $conf = new Conf();
        $conf->set('bootstrap.servers', EvrazKafkaSettings::get("KAFKA_URL_BROKER"));
        $conf->set('sasl.username', EvrazKafkaSettings::get($this->getType() . "_USER_NAME"));
        $conf->set('sasl.password', EvrazKafkaSettings::get($this->getType() . "_PASSWORD"));
        $conf->set('sasl.mechanisms', EvrazKafkaSettings::get("KAFKA_MECHANISM"));
        $conf->set('security.protocol', EvrazKafkaSettings::get("KAFKA_PROTOCOL"));
        $conf->set('ssl.ca.location', EvrazKafkaSettings::get("KAFKA_CERT_LOCATION"));
        return $conf;
    }

    /**
     * @param string $sType
     * @return void
     */
    public function setType(string $sType) : void
    {
        $this->sTypeConnect = $sType;
    }

    /**
     * @return string
     */
    public function getType() : string
    {
        return $this->sTypeConnect;
    }

}