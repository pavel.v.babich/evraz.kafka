<?php

namespace Evraz\Kafka\Services;

use RdKafka\Conf;
use RdKafka\KafkaConsumer;
use Evraz\Kafka\Services\Contracts\ConsumerInterface;

/**
 * @class KConsumer
 */
class KConsumer
{

    public $consumer;

    /**
     * @param Conf $conf
     * @param string $sGroupId
     * @param array $arTopics
     * @param string $sEarliest
     * @param string $sPartitionEOF
     * @throws \Exception
     */
    public function __construct(Conf $conf, string $sGroupId, array $arTopics, string $sEarliest = 'earliest', string $sPartitionEOF = 'true')
    {
        $conf->setRebalanceCb(function (KafkaConsumer $kafka, $err, array $partitions = null) {
            switch ($err) {
                case RD_KAFKA_RESP_ERR__ASSIGN_PARTITIONS:
                    $kafka->assign($partitions);
                    break;

                case RD_KAFKA_RESP_ERR__REVOKE_PARTITIONS:
                    $kafka->assign(NULL);
                    break;

                default:
                    throw new \Exception($err);
            }
        });
        $conf->set('group.id', $sGroupId);
        $conf->set('auto.offset.reset', $sEarliest);
        $conf->set('enable.partition.eof', $sPartitionEOF);
        $this->consumer = new KafkaConsumer($conf);
        $this->consumer->subscribe($arTopics);
    }
}