<?
namespace Evraz\Kafka\Services;

use Bitrix\Main\
    {
        Application,
        Entity,
        Config\Option,
        Page\Asset,
        Loader
    };
use Bitrix\Highloadblock,
    Bitrix\Highloadblock\DataManager,
    Bitrix\Highloadblock\HighloadBlockTable,
    Bitrix\Highloadblock\HighloadBlockLangTable;
use Evraz\Kafka\Services\Contracts\Hlblocksinterface;
use \Bitrix\Main\Type\DateTime;

Loader::IncludeModule('highloadblock');

class Hlblocks extends DataManager
{
    private $arHlBlocks = [];

    public $arExhausters = [];

    public $arExhaustersMapping = [];

    public $LastErrors = [];

    public function __construct()
    {
        $this->arHlBlocks = $this->GetHlBlocks();
        $this->arExhausters = $this->GetExhausters();
        $this->arExhaustersMapping = $this->GetExhaustersMapping();
    }

    private function GetHlBlocks(){
        $arHlBlocks = [];
        $arData = HighloadBlockTable::getList([
            'filter' => []
        ])->fetchAll();
        foreach($arData as $arHL){
            $entity = HighloadBlockTable::compileEntity($arHL);
            $arHL['CLASS'] = $entity->getDataClass();
            $arHlBlocks[$arHL['TABLE_NAME']] = $arHL;
        }
        return $arHlBlocks;
    }

    public function LoadHlExhauster($arSheetNames){
        foreach($arSheetNames as $sheetKey => $sheetName){
            $sheetCode = \Cutil::translit($sheetName, "ru");
            if(!isset($this->arExhausters[$sheetCode])){
                $this->CreateExhauster([
                    "CODE" => $sheetCode,
                    "NAME" => $sheetName,
                ]);
            }
        }
    }

    public function GetExhausters(){
        $arExhausters = [];
        $ExhaustersClass = $this->arHlBlocks['exhausters']['CLASS'];
        $rsExhausters = $ExhaustersClass::getList([
             "select" => ["*"]
        ]);
        while($arExhauster = $rsExhausters->fetch()){
            if($arExhauster['UF_DATE']){
                $arExhauster['UF_DATE_FORMAT'] = $arExhauster['UF_DATE']->format('d.m.Y');
                $timestamp = $arExhauster['UF_DATE']->getTimestamp();
                $arExhauster['CONTINUOUS_WORK'] = round((time() - $timestamp) / (3600 * 24));
            }
            $arExhausters[$arExhauster['UF_CODE']] = $arExhauster;
        }
        return $arExhausters;
    }

    public function GetExhaustersMapping(){
        $arExhaustersMapping = [];
        $ExhaustersMappingClass = $this->arHlBlocks['exhaustermapping']['CLASS'];
        $rsExhaustersMapping = $ExhaustersMappingClass::getList([
             "select" => ["*"]
        ]);
        while($arExhausterMapping = $rsExhaustersMapping->fetch()){
            $arExhaustersMapping[$arExhausterMapping['UF_KAFKA_CODE']] = $arExhausterMapping;
        }
        return $arExhaustersMapping;
    }

    private function CreateExhauster(array $arExhauster){
        $ExhaustersClass = $this->arHlBlocks['exhausters']['CLASS'];
        $result = $ExhaustersClass::add([
            'UF_NAME' => $arExhauster['NAME'],
            'UF_CODE' => $arExhauster['CODE'],
        ]);
        if($result->isSuccess()){
            $this->arExhausters[$arExhauster['CODE']] = [
                'ID' => $result->getId(),
                'UF_NAME' => $arExhauster['NAME'],
                'UF_CODE' => $arExhauster['CODE']
            ];
        }
    }

    public function LoadExhausterMapping(array $arExhausterMapping){
        if(!empty($arExhausterMapping)){
            $arData = [];
            foreach($arExhausterMapping as $arMappingItem){
                if(!isset($this->arExhaustersMapping[$arMappingItem['UF_KAFKA_CODE']])){
                    $arData[] = $arMappingItem;
                }
            }
            if(!empty($arData)){
                $ExhaustersMappingClass = $this->arHlBlocks['exhaustermapping']['CLASS'];
                $result = $ExhaustersMappingClass::addMulti($arData);
                if (!$result->isSuccess()) {
                    $this->LastErrors[] = 'Ошибка добавления Мэппинга';
                }
            }
        }
    }

    public function saveData(array $arData){
        if($arData['moment']){
            $objDateTime = new DateTime($arData['moment'], "Y-m-d\TH:i:s");
            $arDateTime = [
                'UF_DATE' => $objDateTime
            ];
            $DateTimeClass = $this->arHlBlocks['exhausterdatadatetime']['CLASS'];
            $result = $DateTimeClass::add($arDateTime);
            if($result->isSuccess()){
                $dateId = $result->getId();
                $arExhausterData = [];
                foreach($arData as $key => $value){
                    if($key !== 'moment'){
                        $arExhausterData[] = [
                            "UF_DATE_ID" => $dateId,
                            "UF_KAFKA_CODE" => $key,
                            "UF_VALUE" => $value,
                        ];
                    }
                }

                if(!empty($arExhausterData)){
                    $ExhausterDataClass = $this->arHlBlocks['exhausterdata']['CLASS'];
                    $result = $ExhausterDataClass::addMulti($arExhausterData);
                    if (!$result->isSuccess()) {
                        $this->LastErrors[] = 'Ошибка добавления данных '.$arData['moment'];
                    }
                }
            } else {
                $this->LastErrors[] = 'Ошибка добавления Даты и времени в базу';
            }
        }else{
            $this->LastErrors[] = "Данные не распознаны!";
        }
    }

    public function getDatesPeriod($fromDay){
        $arDateTmes = [];
        $DateTimeClass = $this->arHlBlocks['exhausterdatadatetime']['CLASS'];
        $rsDateTime = $DateTimeClass::getList([
             "select" => ["*"],
             "filter" => [">UF_DATE" => $fromDay],
             "order" => ["ID" => "ASC"],
        ]);
        while($arDateTmeItem = $rsDateTime->fetch()){
            $arDateTmeItem['TIMESTAMP'] = $arDateTmeItem['UF_DATE']->getTimestamp();
            $arDateTmes[$arDateTmeItem['ID']] = $arDateTmeItem;
        }
        return $arDateTmes;
    }

    public function getLastData(){
        $arData = [];
        $DateTimeClass = $this->arHlBlocks['exhausterdatadatetime']['CLASS'];
        $arDateTime = $DateTimeClass::getList([
             "select" => ["*"],
             "order" => ["ID" => "DESC"],
             "limit" => 1
        ])->fetch();
        if($arDateTime['ID']){
            $arData['DATETIME'] = $arDateTime['UF_DATE']->format('d.m.Y H:i:s');
            $arData['DATA'] = $this->GetExhaustersData($arDateTime['ID']);
        }
        return $arData;
    }

    public function GetExhaustersData(int $dateId){
        $arData = [];
        $ExhausterDataClass = $this->arHlBlocks['exhausterdata']['CLASS'];
        $rsData = $ExhausterDataClass::getList([
            'select' => ['*'],
            'filter' => ['UF_DATE_ID' => $dateId]
        ]);
        while($arItem = $rsData->fetch()){
            $arData[] = $arItem;
        }
        return $arData;
    }

    public function getDayDateID($fromDate){
        $arData = [];
        $DateTimeClass = $this->arHlBlocks['exhausterdatadatetime']['CLASS'];
        $arDateTime = $DateTimeClass::getList([
             "select" => ["*"],
             "order" => ["ID" => "ASC"],
             "filter" => [">UF_DATE" => $fromDate],
             "limit" => 1
        ])->fetch();
        return $arDateTime['ID'];
    }

    public function getSignal($DateID, $kafkaCode){
        $arData = [];
        $ExhausterDataClass = $this->arHlBlocks['exhausterdata']['CLASS'];
        $kafkaCode = addslashes($kafkaCode);
        $rsData = $ExhausterDataClass::getList([
            'select' => ['*'],
            'filter' => [
                'UF_DATE_ID' => $DateID,
                'UF_KAFKA_CODE' => $kafkaCode
            ]
        ]);
        while($arItem = $rsData->fetch()){
            $arData[] = $arItem;
        }
        return $arData;
    }

    public function NormalizeExhaustersData(array $arData)
    {
        $arResult = $this->arExhausters;
        foreach($arResult as &$arExhauster){
            foreach($arData as $arItem){
                $exhausterId = $this->arExhaustersMapping[$arItem['UF_KAFKA_CODE']]['UF_EXHAUSTER'];
                if($arExhauster['ID'] == $exhausterId){
                    $deviceName = $this->arExhaustersMapping[$arItem['UF_KAFKA_CODE']]['UF_DEVICE_NAME'];
                    $ParamName = $this->arExhaustersMapping[$arItem['UF_KAFKA_CODE']]['UF_PARAM_NAME'];
                    $arChar = [
                        'UF_PARAM_CHAR' => $this->arExhaustersMapping[$arItem['UF_KAFKA_CODE']]['UF_PARAM_CHAR'],
                        'UF_CODE' => $this->arExhaustersMapping[$arItem['UF_KAFKA_CODE']]['UF_CODE'],
                        'UF_KAFKA_CODE' => $arItem['UF_KAFKA_CODE'],
                        'UF_NAME' => $this->arExhaustersMapping[$arItem['UF_KAFKA_CODE']]['UF_NAME'],
                        'UF_TYPE' => $this->arExhaustersMapping[$arItem['UF_KAFKA_CODE']]['UF_TYPE'],
                        'UF_ACTIVE' => $this->arExhaustersMapping[$arItem['UF_KAFKA_CODE']]['UF_ACTIVE'],
                        'VALUE' => $arItem['UF_VALUE']
                    ];
                    if(mb_strtolower($ParamName) === 'вода' ||
                      mb_strtolower($ParamName) === 'масло' ||
                      !$ParamName
                    ){
                        $arExhauster['DEVICES'][$deviceName][$arChar['UF_CODE']] = $arChar;
                    } elseif(mb_strtolower($ParamName) === 'вибрация') {
                        $ParamName = $ParamName . " " . mb_strtolower($arChar['UF_PARAM_CHAR']);
                        $arExhauster['DEVICES'][$deviceName][$ParamName][$arChar['UF_CODE']] = $arChar;
                    } else {
                        $arExhauster['DEVICES'][$deviceName][$ParamName][$arChar['UF_CODE']] = $arChar;
                    }
                }
            }
        }
        unset($arExhauster);
        return $arResult;
    }

    public function getParamsData($fromDay, $arSetParams){
        $arParamsDatas = [];
        $arSetParamsKeys = [];
        foreach($arSetParams as $paramCode){
            $arSetParamsKeys[$paramCode] = true;
        }
        $arDates = $this->getDatesPeriod($fromDay);
        $arDateTimeIds = array_column($arDates, 'ID');

        $arParamsDatas = [];
        $ExhausterDataClass = $this->arHlBlocks['exhausterdata']['CLASS'];
        $rsParamsData = $ExhausterDataClass::getList([
            'select' => ['*'],
            'filter' => [
                'UF_DATE_ID' => $arDateTimeIds
            ],
            'order' => ['ID' => 'asc']
        ]);
        while($arItem = $rsParamsData->fetch()){
            if($arSetParamsKeys[$arItem['UF_KAFKA_CODE']]){
                $arItem['TIMESTAMP'] = $arDates[$arItem['UF_DATE_ID']]['TIMESTAMP'];
                $arParamsDatas[$arItem['UF_KAFKA_CODE']][$arItem['UF_DATE_ID']] = $arItem;
            }
        }
        return $arParamsDatas;
    }

    public function UpdateEksgausterPrognoz($EksgausterID, $maxDays){
        $ExhaustersClass = $this->arHlBlocks['exhausters']['CLASS'];
        $rsExhausters = $ExhaustersClass::update($EksgausterID, [
             "UF_PROGNOZ" => $maxDays
        ]);
    }
}