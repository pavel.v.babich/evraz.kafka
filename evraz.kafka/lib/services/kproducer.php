<?php

namespace Evraz\Kafka\Services;

use RdKafka\Conf;
use RdKafka\Producer;
use Evraz\Kafka\Services\Contracts\ProducerInterface;

/**
 * @class KProducer
 *
 * @see send(array $arMessage, string $sKey) - Отправка сообщения в брокер
 */
class KProducer implements ProducerInterface
{

    /**
     * @var array
     */
    private $sTopic;

    /**
     * @var RdKafka\Producer
     */
    private $oProducer;

    /**
     * @var object
     */
    private $oTopic;

    /**
     * @var int
     */
    private $iPool = 0;

    /**
     * @var int
     */
    private $iFlushTime = 10000;

    /**
     * @var int
     */
    private $iMsgFlags = 0;

    /**
     * @param Conf $conf
     * @param string $sTopic
     */
    public function __construct(Conf $conf, string $sTopic)
    {
        $this->sTopic = $sTopic;
        $this->oProducer = new Producer($conf);
        $this->oTopic = $this->oProducer->newTopic($this->sTopic);
    }

    /**
     * Отправка сообщения в брокер
     * @param array $arMessage
     * @param string $sKey
     * @return void
     */
    public function send(array $arMessage, string $sKey) : void
    {
        $this->oTopic->produce(RD_KAFKA_PARTITION_UA, $this->iMsgFlags, json_encode($arMessage), $sKey);
        $this->oProducer->poll($this->iPool);
        $result = $this->oProducer->flush($this->iFlushTime);
        if (RD_KAFKA_RESP_ERR_NO_ERROR !== $result) {
            throw new \RuntimeException(GetMessage("EVRAZ_KAFKA_TIME_OUT_MESSAGE"));
        }
    }

}

//$conf = new RdKafka\Conf();
//$conf->set('bootstrap.servers', 'rc1b-c3vsb3vu736nj4fs.mdb.yandexcloud.net:9091'); // metadata.broker.list bootstrap.servers
//$conf->set('security.protocol', 'SASL_SSL');
//$conf->set('sasl.mechanisms', 'SCRAM-SHA-512');
//$conf->set('sasl.username', 'evraz_market_producer');
//$conf->set('sasl.password', 'Qw-@#^_12-N');
//$conf->set('ssl.ca.location', '/usr/local/share/ca-certificates/Yandex/YandexCA.crt');
//$producer = new RdKafka\Producer($conf);
//
//$topic = $producer->newTopic("evraz.market");
//
//$iMaxMessages = 2;
//
//for ($i = 0; $i < $iMaxMessages; $i++) {
//    $topic->produce(RD_KAFKA_PARTITION_UA, 0, "RG: Testing message $i", "TEST");
//    $producer->poll(0);
//}
//
//for ($flushRetries = 0; $flushRetries < $iMaxMessages; $flushRetries++) {
//    $result = $producer->flush(10000);
//    if (RD_KAFKA_RESP_ERR_NO_ERROR === $result) {
//        break;
//    }
//}
//
//if (RD_KAFKA_RESP_ERR_NO_ERROR !== $result) {
//    throw new \RuntimeException('Не удалось отправить. Сообщения могли быть потеряны!');