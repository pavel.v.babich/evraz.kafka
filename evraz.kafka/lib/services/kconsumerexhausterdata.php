<?php

namespace Evraz\Kafka\Services;

use phpDocumentor\Reflection\Types\Boolean;
use RdKafka\Conf;
use RdKafka\KafkaConsumer;
use RdKafka\TopicPartition;
use Evraz\Kafka\Services\Contracts\ConsumerInterface;
use Evraz\Kafka\Services\Hlblocks;
use Evraz\Main\Helper\CacheHelper;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Web\Json;

/**
 * @class KConsumerExhausterData
 *
 * @see read(KafkaConsumer $oConsumer) - Чтение данных из топика
 * @see saveToLog() - Сохранение данных в логи
 * @see getTopicName() - Геттер наименования топика
 */
class KConsumerExhausterData implements ConsumerInterface
{
    /**
     * @var float|int
     */
    private $iMsTimeForConsume = 5 * 1000;

    /**
     * @var string
     */
    public $defaultTopicName = "zsmk-9433-dev-01";

    /**
     * @var array
     */
    public $arPriceTypes = [];

    /**
     * Чтения из топика
     *
     * @param KafkaConsumer $oConsumer
     * @return array|null
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    public function read(KafkaConsumer $oConsumer) : ? array
    {
        $arResult = [];
        $topicName = $this->getTopicName();
        $curOffset = intval(Option::get("evraz.kafka", "EXHAUSTER_OFFSET"));
        $topicPartition = new TopicPartition($topicName, 0, $curOffset);
        try{
            $oConsumer->commit([$topicPartition]);
        }
        catch (\Exception $e) {
            // Ошибка установки оффсета. Скорее всего подключение занято или ещё не освободилось
            // echo "Ошибка установки оффсета. Скорее всего подключение занято или ещё не освободилось";
            $connection = false;                      
        }

        if($connection !== false) {
            $start = microtime(true);
            $HlBlockHandler = new Hlblocks();
            while ( round(microtime(true) - $start) < 60 ) {
                $message = $oConsumer->consume($this->iMsTimeForConsume);
                switch ($message->err) {
                    case RD_KAFKA_RESP_ERR_NO_ERROR:
                        $payload = $message->payload;
                        $timestamp = intval($message->timestamp / 1000);
                        if($payload) {
                            try{
                                $arData = Json::decode($payload);
                                if (is_array($arData) && !empty($arData)) {
                                    $HlBlockHandler->saveData($arData);
                                }
                            } catch (\Exception $e) {
                                // Не могу расшифровать сообщение
                            }
                        }
                        $offset = $message->offset;
                        if($offset) {
                            $offset++;
                            Option::set("evraz.kafka", "EXHAUSTER_OFFSET", $offset);
                        }
                        break 1;
                    case RD_KAFKA_RESP_ERR__PARTITION_EOF:
                        // Нет больше сообщений; ожидаем сообщений
                        break 1;
                    case RD_KAFKA_RESP_ERR__TIMED_OUT:
                        // Время истекло
                        break 2;
                    default:
                        throw new \Exception($message->errstr(), $message->err);
                }
            }
        }
        return $arResult;
    }

    /**
     * Геттер наименования топика
     * @return string|null
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    public function getTopicName() : ? string
    {
        return trim(\EvrazKafkaSettings::get("KAFKA_TOPIC_PRICE_LIST")) ? trim(\EvrazKafkaSettings::get("KAFKA_TOPIC_PRICE_LIST")) : $this->defaultTopicName;
    }

    /**
     * Сохранение лог файла
     * @param string $message
     * @return void
     */
    private function saveToLog( string $message ) : void
    {
        $dir = $_SERVER['DOCUMENT_ROOT'] . '/upload/kafka_log';
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        file_put_contents(
            $dir . "/kafka_" . date('d-m-Y') . ".log",
            date('d-m-Y H:i:s') . '; ' . $message . PHP_EOL,
            FILE_APPEND
        );
        $logs = array_diff(scandir($dir), ['..', '.']);
        foreach ($logs as $file){
            if(file_exists($dir . '/' . $file)){
                $date = str_replace(['kafka_', '.log'], '', $file);
                $stmp = MakeTimeStamp($date, "DD-MM-YYYY");
                if( time() - 3600 * 24 * 10 > $stmp){
                    unlink($dir . '/' . $file);
                }
            }
        }
    }
}
