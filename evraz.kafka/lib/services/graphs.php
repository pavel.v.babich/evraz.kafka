<?php
namespace Evraz\Kafka\Services;

use Bitrix\Main\Loader;
use Evraz\Kafka\Services\Hlblocks;

/**
 * @class Excel
 *
 * @package Evraz\Kafka\Services
 */
class Graphs
{
    public function __construct()
    {

    }

    /**
    * Загрузка Данных для графика
    */
    public function getGraphJson($arSetParams = [], $zoom)
    {
        $arZoomPeriod = [
            "1" => 600,
            "2" => 3600,
            "3" => 3600 * 4,
            "4" => 3600 * 16,
            "5" => 3600 * 24
        ];
        if(!isset($arZoomPeriod[$zoom])){
            $zoom = 1;
        }
        $arResult = [];
        $HlBlockHandler = new Hlblocks();
        $fromDay = date('d.m.Y H:i:s', time() - $arZoomPeriod[$zoom] - 3600 * 4);
        $arResult["period"] = substr($fromDay,0,16) . "&nbsp;-&nbsp;" . date('d.m.Y H:i');
        $arResult["DATA"] = $HlBlockHandler->getParamsData($fromDay, $arSetParams);

        return $arResult;
    }
}