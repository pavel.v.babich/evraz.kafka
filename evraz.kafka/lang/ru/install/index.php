<?php
$MODULE_ID = 'evraz.kafka';
$MESS[$MODULE_ID . '_MODULE_NAME'] = 'Модуль с функционалом кафки evraz.market';
$MESS[$MODULE_ID . '_MODULE_DESCRIPTION'] = 'Модуль содержащий основные классы и методы по работе с кафкой';
$MESS[$MODULE_ID . '_PARTNER_NAME'] = 'EVRAZ';
$MESS[$MODULE_ID . '_PARTNER_URI'] = "https://evraz.market/";
$MESS[$MODULE_ID . '_MODULE_NOT_INSTALLED'] = "Модуль не установлен";
$MESS[$MODULE_ID . '_INSTALL_ERROR_VERSION'] = "Версия битрикс неподдерживает данный модуль";
