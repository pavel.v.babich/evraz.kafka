<?php
$MESS["PS_MAIN_MODULE_PAGE_SETTINGS"] = "Дополнительные настройки EVRAZ";
$MESS["ACCESS_DENIED"] = "Доступ запрещён";
$MESS["PS_MODULE_KAFKA_TITLE"] = "Данные для функционирования сервиса kafka";
$MESS["PS_MODULE_KAFKA_URL_BROKER"] = "Адрес брокера (URL:PORT)";
$MESS["PS_MODULE_KAFKA_MECHANISM"] = "Механизм подключения (GSSAPI, PLAIN, SCRAM-SHA-256, SCRAM-SHA-512, OAUTHBEARER)";
$MESS["PS_MODULE_KAFKA_PROTOCOL"] = "Протокол (SASL_PLAINTEXT, SASL_SSL)";
$MESS["PS_MODULE_KAFKA_CERT_LOCATION"] = "Путь к сертификату (*.crt)";
$MESS["PS_MODULE_CONSUMER_USER_NAME"] = "Логин консьюмера";
$MESS["PS_MODULE_CONSUMER_PASSWORD"] = "Пароль консьюмера";
$MESS["PS_MODULE_CONSUMER_LOG_ACTIVE"] = "Включить логирование";
$MESS["PS_MODULE_CONSUMER_LOG_FILE"] = "Файлы лога";
$MESS["PS_MODULE_KAFKA_TOPIC_PRICE_LIST"] = "Наименование топика по обмену цен";
$MESS["PS_MODULE_KAFKA_GROUP_ID_PRICE_LIST"] = "Group.id для топика по обмену цен";
