<?
$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../../..");
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;
Loader::includeModule("evraz.kafka");

use Evraz\Kafka\Services\{
    Auth,
    KConsumer,
    KConsumerExhausterData
};
use Bitrix\Main\ArgumentNullException;
use Bitrix\Main\ArgumentOutOfRangeException;
use Bitrix\Main\Config\Configuration;

$oAuth = new Auth("CONSUMER");
$oConf = $oAuth->register();
$oConsumer = new KConsumerExhausterData();
$sGroupId = trim(\EvrazKafkaSettings::get("KAFKA_GROUP_ID_PRICE_LIST")) ? trim(\EvrazKafkaSettings::get("KAFKA_GROUP_ID_PRICE_LIST")) : self::getDefaultGroupID();
$oRegConsumer = new KConsumer($oConf, $sGroupId, [$oConsumer->getTopicName()]);
$consumer = $oRegConsumer->consumer;
$oConsumer->read($consumer);

$curOffset = intval(Bitrix\Main\Config\Option::get("evraz.kafka", "EXHAUSTER_OFFSET"));
file_put_contents(
    $_SERVER['DOCUMENT_ROOT']."/crontab.log",
    "date=".date("d.m.Y H:i:s")."; " .
    "curOffset=".$curOffset . PHP_EOL,
    FILE_APPEND
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");