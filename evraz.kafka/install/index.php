<?php

use \Bitrix\Main\Application,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Main\ModuleManager,
    \Bitrix\Main\LoaderException;

class evraz_kafka extends CModule
{
    var $MODULE_ID = "evraz.kafka";
    var $PARTNER_NAME = "EVRAZ";
    var $PARTNER_URI = "https://evraz.market/";

    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $PARTNER_ID = "EVRAZ";
    var $MODULE_SORT = 100;
    var $SHOW_SUPER_ADMIN_GROUP_RIGHTS;
    var $MODULE_GROUP_RIGHTS;

    protected $module_dir = '';
    protected $isLocal = '';
    protected $exclusionAdminFiles = array();

    /**
     * Конструктор модуля evraz.kafka
     */
    public function __construct()
    {
        $arModuleVersion = array();
        include(dirname(__FILE__) . '/version.php');

        $this->module_dir = Loader::getLocal('modules/' . $this->MODULE_ID);

        $this->isLocal = !!strpos($this->module_dir, '/local/modules/');

        $this->MODULE_NAME = Loc::getMessage($this->MODULE_ID . '_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage($this->MODULE_ID . '_MODULE_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage($this->MODULE_ID . '_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage($this->MODULE_ID . '_PARTNER_URI');
        $this->MODULE_VERSION = empty($arModuleVersion['VERSION']) ? '' : $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = empty($arModuleVersion['VERSION_DATE']) ? '' : $arModuleVersion['VERSION_DATE'];
        $this->SHOW_SUPER_ADMIN_GROUP_RIGHTS = "Y";
        $this->MODULE_GROUP_RIGHTS = "Y";
    }

    /**
     * Проверим версию установленной версии битрикс
     * @return bool
     */
    function isVersionD7()
    {
        return CheckVersion(ModuleManager::getVersion('main'), '14.00.00');
    }

    function isModule($module_id = "iblock")
    {
        return ModuleManager::isModuleInstalled($module_id);
    }

    /**
     * @throws LoaderException
     */
    protected function checkModules()
    {
        if (!Loader::includeModule('evraz.main')) {
            // исключение в загрузчике
            throw new LoaderException(Loc::getMessage($this->MODULE_ID . '_MODULE_NOT_INSTALLED'));
        };

    }

    /**
     * str_ireplace — Регистронезависимый вариант функции str_replace()
     * @param bool $notDocumentRoot
     * @return mixed|string
     */
    function getPath($notDocumentRoot = false)
    {
        if ($notDocumentRoot) {
            return str_ireplace(Application::getDocumentRoot(), '', dirname(__DIR__));
        } else {
            return dirname(__DIR__);
        }
    }


    /**
     * @return bool
     */
    public function DoInstall()
    {
        global $APPLICATION;
        if ($this->isVersionD7()) {
            ModuleManager::registerModule($this->MODULE_ID);
        } else {
            $APPLICATION->ThrowException(Loc::getMessage($this->MODULE_ID . "_INSTALL_ERROR_VERSION"));
        }
        return true;
    }

    /**
     * @return bool
     */
    public function DoUninstall()
    {
        ModuleManager::unRegisterModule($this->MODULE_ID);
        return true;
    }
}
